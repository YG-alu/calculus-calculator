package Main.Derivative;

import Main.Var;
import Main.Equation;

public class Main {

    private Var x;
    private Equation equation;

    public Main(Var x, Equation equation) {
        this.x = x;
        this.equation = equation;
    }

    public Equation MainLoop() {
        return DeltaXDiv(DeltaY());
    }

    public Equation DeltaY() {
        Equation FOfX = equation;
        Equation FOfXDeltaX = equation.setVar('x', 'd');
        return FOfXDeltaX.sub(FOfX);
    }

    public Equation DeltaXDiv(Equation DeltaY) {
        if (DeltaY.CancelAble()) {
            return DeltaY.DivCancel('d');
        } else {
            return DeltaY.retInf();
        }
    }
}
