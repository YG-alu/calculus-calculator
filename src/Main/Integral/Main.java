package Main.Integral;

import Main.Function;
import java.util.Scanner;

public class Main {
    public final double increment = 1E-4;

    public double integrate(double a, double b, Function function) {
        double area = 0;
        double boundSwap = 1;
        if (a > b) {
            Scanner input = new Scanner(System.in);
            System.out.println("Bound a is grater than bound b, do you want to swap them? [yes/no]");
            String YOrN = input.nextLine();
            if (YOrN.equals("no")) {
                double newA = a;
                a = b;
                b = newA;
                boundSwap = -1;
            }
        }

        for (double i = a * increment; i < b; i += increment) {
            // area = (increment / 2) * (function.f(i) - function.f(i - increment));
            double dist = i - a;
            area += (increment / 2) * (function.f(a + dist) + function.f(a + dist - increment));
        }

        return area * boundSwap;
    }

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println(main.integrate(0, 5, x -> 5*x + 3*Math.pow(x, 2)));
    }
}
